<?php

return [

    /**
     * Prefix for the routes. Rarely changed but useful if prefix isn't available.
     */
    'prefix' => 'qualtrics-wh',

    /**
     * The base URL for publication URLs. Can be changed to an external URL (useful for testing).
     */
    'url' => env('QUALTRICS_WEBHOOKS_URL', null),

    /**
     * Set to true to encrypt the request.
     */
    'encrypted' => env('QUALTRICS_WEBHOOKS_ENCRYPTED', false),

    /**
     * The key to use when decrypting the request body from the notification when "encrypt" is true. 
     */
    'shared_key' => env('QUALTRICS_WEBHOOKS_SHARED_KEY', null),

    /**
     * Events that we are listening to.
     * Each entry must contain the following keys:
     *    event: The name of event in Qualtrics
     *    event_class: The name of the event object that will be fired/dispatched
     *    path: The path to be used in routing. Allowed characters are alphabet, numbers, dash, and underscores.
     */
    'events' => [
        [
            'event' => 'controlpanel.activateSurvey',
            'event_class' => Vector5\LaravelQualtricsWebhooks\Events\Survey\Activate::class,
            'path' => 'survey-active'
        ],
        [
            'event' => 'controlpanel.deactivateSurvey',
            'event_class' => \Vector5\LaravelQualtricsWebhooks\Events\Survey\Deactivate::class,
            'path' => 'survey-deactivate'
        ],            
        [
            'event' => 'surveyengine.startedRecipientSession',
            'event_class' => Vector5\LaravelQualtricsWebhooks\Events\Survey\StartedRecipientSession::class,
            'path' => 'survey-started-recipient-session'
        ],
        [
            'event' => 'surveyengine.partialResponse',
            'event_class' => Vector5\LaravelQualtricsWebhooks\Events\Survey\PartialResponse::class,
            'path' =>'survey-partial-response',
        ],
        [
            'event' => 'surveyengine.completedResponse',
            'event_class' => Vector5\LaravelQualtricsWebhooks\Events\Survey\CompletedResponse::class,
            'path' => 'survey-completed-response'
        ]
    ],

    /**
     * The controller to handle the notification.
     * Can be changed to a class but it should extend the default controller.
     * 
     * See \Vector5\LaravelQualtricsWebhooks\Http\NotificationController for additional info.
     */
    'controller' => Vector5\LaravelQualtricsWebhooks\Http\NotificationController::class

];