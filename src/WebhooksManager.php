<?php

namespace Vector5\LaravelQualtricsWebhooks;

use InvalidArgumentException;
use Illuminate\Routing\UrlGenerator as LaravelUrlGenerator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class WebhooksManager
{
    /**
     * @var array
     */
    protected $config;

    /**
     * @var array
     */
    protected $events;

    /**
     * @var \Illuminate\Routing\UrlGenerator 
     */
    protected $url;

    /**
     * Create new webhooks manager instance.
     * 
     * @param array $config
     * @param \Illuminate\Routing\UrlGenerator $url
     */
    public function __construct(array $config, LaravelUrlGenerator $url)
    {
        $this->config = $config;
        $this->events = (new Collection($config['events']))->keyBy('event');
        $this->url = $url;
    }

    /**
     * Get the publication URL for the given event name.
     * 
     * @param string $event
     * @param array $parameters
     * @return string
     */
    public function publicationUrl($event, array $parameters = [])
    {
        $path = $this->getEventConfig($event)['path'];

        // Use a different base URL if it's explicitly set.
        if ($baseUrl = $this->getConfig('url')) {
            $eventParam = 'event='.urlencode($path);

            // Check if there is an {event} token on the custom URL. Otherwise append as query parameter
            if (strpos($baseUrl, '{event}') !== false) {
                return str_replace('{event}', $path, $baseUrl);
            } 

            $baseUrl = rtrim($baseUrl, '/');
            
            return $baseUrl.(strpos($baseUrl, '?') !== false ? '&' : '?').$eventParam;
        }

        return $this->url->route('qualtrics-webhooks.listen', array_merge(['event' => $path], $parameters));
    }

    /**
     * Get the configuration for the specified event.
     * 
     * @param string $event
     * @param bool $throw (optional)
     * @return array|null
     * 
     * @throws \InvalidArgumentException
     */
    public function getEventConfig($event, $throw = true)
    {
        if ($this->events->has($event)) {
            return $this->events->get($event);
        }

        if ($throw) {
            throw new InvalidArgumentException("Could not find config for webhook event $event");
        } 

        return null;
    }

    /**
     * Get the event name by it's path value.
     * 
     * @param string $path
     * @return string|null
     */
    public function getEventByPath($path)
    {
        return ($config = $this->events->firstWhere('path', $path)) ? $config['event'] : null;
    }

    /**
     * Get the configuration with the specified key.
     * 
     * @param string $key
     * @param mixed $default (optional)
     * @return mixed
     */
    public function getConfig($key, $default = null)
    {
        return Arr::get($this->config, $key, $default);
    }
}