<?php

namespace Vector5\LaravelQualtricsWebhooks\Events;

use Illuminate\Http\Request;

class NotificationEvent
{
    /**
     * @var array
     */
    public $data;

    /**
     * @var \Illuminate\Http\Request
     */
    public $request;

    /**
     * Create new event instance.
     * 
     * @param object $data
     * @param \Illuminate\Http\Request $request
     */
    public function __construct($data, Request $request)
    {
        $this->data = $data;
        $this->request = $request;
    }
}