<?php

namespace Vector5\LaravelQualtricsWebhooks\Events\Survey;

use Vector5\LaravelQualtricsWebhooks\Events\NotificationEvent;

class Deativate extends NotificationEvent
{
}