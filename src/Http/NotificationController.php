<?php

namespace Vector5\LaravelQualtricsWebhooks\Http;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vector5\LaravelQualtricsWebhooks\WebhooksManager;
use Vector5\LaravelQualtricsWebhooks\Concerns\VerifiesRequest;

class NotificationController extends Controller
{
    use VerifiesRequest;

    /**
     * @var \Vector5\LaravelQualtricsWebhooks\WebhooksManager
     */
    protected $webhooks;
    
    /**
     * Create new controller instance.
     * 
     * @param \Vector5\LaravelQualtricsWebhooks\WebhooksManager $webhooks
     */
    public function __construct(WebhooksManager $webhooks)
    {
        $this->webhooks = $webhooks;
    }

    /**
     * Get the "MSG" from the request.
     * 
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    protected function getMessageRaw(Request $request)
    {
        return $request->input('MSG');
    }

    /**
     * Handles the notification from Qualtrics.
     * 
     * @param \Illuminate\Http\Request $request
     * @param string $eventPath
     * @param array $args
     * @return mixed
     * 
     * @throws \Illuminate\Http\Exceptions\HttpResponseException
     */
    public function listen(Request $request, $eventPath)
    {
        if (! ($event = $this->webhooks->getEventByPath($eventPath))) {
            throw new NotFoundHttpException;
        }

        // First we need to make sure that the even we're receiving is actually known.
        $config = $this->webhooks->getEventConfig($event);

        // Check if there's a specific method to handle the received event
        if (method_exists($this, $method = $this->getControllerMethodForEvent($eventPath))) {
            $this->{$method}($request);

            return $this->accepted();
        }

        return $this->handleNotificationFromConfig($config, $request);
    }

    /**
     * Get the custom controller method for the event using its path.
     * 
     * @param string $eventPath
     * @return string
     */
    protected function getControllerMethodForEvent($eventPath)
    {
        return 'handle'.Str::camel(str_replace('-', '_', $eventPath));
    }

    /**
     * Get response for accepted notification request.
     * 
     * @return \Illuminate\Http\Response
     */
    protected function accepted()
    {
        return response()->noContent();
    }

    /**
     * Handle the notification event using the event configuration.
     * 
     * @param array $config
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function handleNotificationFromConfig($config, Request $request)
    {
        $sharedKey = $this->webhooks->getConfig('shared_key');

        if ($this->webhooks->getConfig('encrypted')) {
            $message = $this->verifyEncrypted($request, $sharedKey);
        } elseif ($sharedKey) {
            $message = $this->verifyHashed($request, $sharedKey);
        } elseif (! ($message = $this->getMessageRaw($request))) {
            $this->unverified();
        }

        $data = json_decode($message);
        $class = $config['event_class'];

        // Simply fire the event so it can be handled anywhere.
        event(new $class($data, $request));
        
        logger('fired webhook notification '.$class);

        return $this->accepted();
    }
}